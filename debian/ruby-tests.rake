require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']

  # stuff that is not available in Debian as of 2018-07-14
  excludes = [
    'test/test_babel_processor.rb',
    'test/test_eco_processor.rb',
    'test/test_closure_compressor.rb',
    'test/test_yui_compressor.rb',
    'test/test_uglifier_compressor.rb',
    'test/test_jsminc_compressor.rb',
    'test/test_coffee_script_processor.rb',
    'test/test_require.rb',
    'test/test_environment.rb',
    'test/test_source_maps.rb',
    'test/test_exporting.rb',
    'test/test_asset.rb',
    'test/test_manifest.rb',
    'test/test_rake_task.rb',
  ]
  exclude_tests = ['--exclude=/eco|closure|yui/']

  t.test_files = FileList['test/**/test_*.rb'] - excludes + exclude_tests
end
